# Variables definition
CC = g++									# compiler variable
CFLAGS = -c          				# flag used in compiling and creating object files	
LFLAGS = -o       				  	# linking flag

EXAMPLE = example_main.o example.o

# All targets 
all: run

# target to generate executable file.

example: $(EXAMPLE)
	$(CC) $(EXAMPLE) $(LFLAGS) example

# target to run executable file
run: example
	./example

# dependencies of example_main.cpp
example_main.o: example_main.cpp example.h
	$(CC) $(CFLAGS) example_main.cpp

# dependencies of example.cc
example.o: example.cc example.h
	$(CC) $(CFLAGS) example.cc

# to destroy all the object and exectuable file
clean:
	rm -f *.o example
